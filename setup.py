from cx_Freeze import setup, Executable

base = None    

executables = [Executable("main.py", base=base)]

packages = ["idna", "threading", "os", "sys", "ctypes", "subprocess", "time", "winreg"]
options = {
    'build_exe': {    
        'packages':packages,
        'includes': ["PyQt5"]
    },    
}

setup(
    name = "Acoustic Calculator",
    options = options,
    version = "0.1151",
    description = 'Program for calculate simple acoustic systems.',
    executables = executables
)