# WinDestr

The open source project simple Windows 10 tool for optimize your pc.

Version 1.0

**Remember. All actions you do at your own peril and risk.**
In case of failure: hosts backup contains in C:\backup, and registry backup contains in LOCAL_DIR\fls

Python version: 3.6.8
Release contains builded program by cx_Freeze.
Libraries used: PyQt5

**Features**
1. Delete standard win 10 apps.
2. Disable Windows Defender.
3. Disable Cortana.
4. Disable Telemetry.
5. Disable Windows Auto Update.

**Delete App list:**
"microsoft.messaging",
"microsoft.windowsmaps",
"microsoft.zunevideo",
"microsoft.windowscommunicationsapps",
"microsoft.windowsstore",
"microsoft.windowsalarms",
"microsoft.storepurchaseapp",
"microsoft.gethelp",
"microsoft.zunemusic",
"microsoft.xboxgamingoverlay",
"microsoft.xboxidentityprovider",
"microsoft.windowssoundrecorder",
"microsoft.microsoftstickynotes",
"microsoft.xboxgameoverlay",
"microsoft.windowscamera",
"microsoft.windowscalculator",
"microsoft.mspaint",
"microsoft.microsoft3dviewer ",
"microsoft.xboxspeechtotextoverlay",
"microsoft.DesktopAppInstaller",
"microsoft.microsoftofficehub",
"microsoft.windowsfeedbackhub",
"microsoft.people",
"microsoft.webmediaextensions",
"microsoft.oneconnect",
"microsoft.xboxidentityprovider",
"microsoft.print3d",
"microsoft.skypeapp",
"microsoft.bingweather",
"microsoft.xboxapp",
"microsoft.xboxspeechtotextoverlay",
"microsoft.storepurchaseapp",
"microsoft.microsoftsolitairecollection",
"microsoft.wallet",
"microsoft.getstarted",
"microsoft.windows.photos",
"microsoft.HEIFImageExtension",
"microsoft.VP9VideoExtensions",
"microsoft.WebpImageExtension",
"microsoft.YourPhone",
"microsoft.ScreenSketch"

You can support project, if i helped you: WebMoney WMID: 064394640485, WMZ: Z859624576379