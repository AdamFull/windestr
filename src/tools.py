import sys, os, ctypes

class Tools:
    @classmethod
    def get_platform(cls):
        return sys.getwindowsversion()
    
    @classmethod
    def isAdmin(cls):
        try:
            is_admin = os.getuid() == 0
        except AttributeError:
            is_admin = ctypes.windll.shell32.IsUserAnAdmin() != 0
        
        return is_admin