import winreg

class reg_ed:
    @classmethod
    
    def isValueExist(cls, key, sub_key, value_name):
        aReg = winreg.ConnectRegistry(None,key)
        try:
            hKey = winreg.OpenKey(aReg, sub_key, 0, winreg.KEY_READ)
            i = 0
            while True:
                try:
                    subvalue = winreg.EnumValue(hKey, i)
                    i += 1
                    if str(subvalue[0]) == value_name:
                        return True
                except WindowsError as e:
                    break
            return False
        except Exception as e:
            winreg.CloseKey(hKey)
            winreg.CloseKey(aReg)
            return False
        pass

    @classmethod
    def edit_registry(cls, key, sub_key, value_name, value):
        aReg = winreg.ConnectRegistry(None,key)
        try:
            hKey = winreg.OpenKey(aReg, sub_key, 0, winreg.KEY_ALL_ACCESS)
        except:
            winreg.CreateKey(aReg, sub_key) #NOT WORKING
            hKey = winreg.OpenKey(aReg, sub_key, 0, winreg.KEY_ALL_ACCESS)
        
        winreg.SetValueEx(hKey, value_name, 0, winreg.REG_DWORD, value)
        print(value_name + "(%s) changed." % (value))
        winreg.CloseKey(hKey)
        winreg.CloseKey(aReg)
