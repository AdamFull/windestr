import os
from src.registry import reg_ed as re
from src.hostscheck import Hosts

class reg_dis:
    @classmethod
    def DisableDefender(cls):
        # Edit Registry: WindowsDefender
        print("Disabling Windows Defender.")
        words = [[18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender", r"AllowFastServiceStartup", 0],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender", r"DisableAntiSpyware", 1],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender", r"ServiceKeepAlive", 0],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender\\Real-Time Protection", r"DisableOAVProtection", 1],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender\\Real-Time Protection", r"DisableRealtimeMonitoring", 1],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender\\Spynet", r"DisableBlockAtFirstSeen", 1],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender\\Spynet", r"LocalSettingOverrideSpynetReporting", 0],
                 [18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender\\Spynet", r"SubmitSamplesConsent", 2]]
        for key, sub_key, name, val in words:
            re.edit_registry(key, sub_key, name, val)
    
    @classmethod
    def DisableCortana(cls):
        print("Disabling Cortana.")
        words = [[18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows\\Windows Search", r"AllowCortana", 0]]
        for key, sub_key, name, val in words:
            re.edit_registry(key, sub_key, name, val)
    
    @classmethod
    def DisableTelemetry(cls):
        print("Disabling Telemetry.")
        commands = ["sc delete DiagTrack", "sc delete dmwappushservice", "echo \"\" > C:\\ProgramData\\Microsoft\\Diagnosis\\ETLLogs\\AutoLogger\\AutoLogger-Diagtrack-Listener.etl"]
        "Software"
        for command in commands:
            result=os.popen(command).read()
            print(result)
        words = [[18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows Defender", r"DisableAntiSpyware", 1]]
        
        for key, sub_key, name, val in words:
            re.edit_registry(key, sub_key, name, val)
        
        Hosts.do()
    
    @classmethod
    def DisableWindowsAU(cls):
        print("Disabling AU.")
        words = [[18446744071562067970, r"SOFTWARE\\Policies\\Microsoft\\Windows\\WinfowsUpdate\\AU", r"AUOptions", 2]]
        
        for key, sub_key, name, val in words:
            re.edit_registry(key, sub_key, name, val)