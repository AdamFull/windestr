import subprocess


class Apps:
	apps_dict = ["microsoft.messaging",
			 "microsoft.windowsmaps",
			 "microsoft.zunevideo",
			 "microsoft.windowscommunicationsapps",
			 "microsoft.windowsstore",
			 "microsoft.windowsalarms",
			 "microsoft.storepurchaseapp",
			 "microsoft.gethelp",
			 "microsoft.zunemusic",
			 "microsoft.xboxgamingoverlay",
			 "microsoft.xboxidentityprovider",
			 "microsoft.windowssoundrecorder",
			 "microsoft.microsoftstickynotes",
			 "microsoft.xboxgameoverlay",
			 "microsoft.windowscamera",
			 "microsoft.windowscalculator",
			 "microsoft.mspaint",
			 "microsoft.microsoft3dviewer ",
			 "microsoft.xboxspeechtotextoverlay",
			 "microsoft.DesktopAppInstaller",
			 "microsoft.microsoftofficehub",
			 "microsoft.windowsfeedbackhub",
			 "microsoft.people",
			 "microsoft.webmediaextensions",
			 "microsoft.oneconnect",
			 "microsoft.xboxidentityprovider",
			 "microsoft.print3d",
			 "microsoft.skypeapp",
			 "microsoft.bingweather",
			 "microsoft.xboxapp",
			 "microsoft.xboxspeechtotextoverlay",
			 "microsoft.storepurchaseapp",
			 "microsoft.microsoftsolitairecollection",
			 "microsoft.wallet",
			 "microsoft.getstarted",
			 "microsoft.windows.photos",
			 "microsoft.HEIFImageExtension",
			 "microsoft.VP9VideoExtensions",
			 "microsoft.WebpImageExtension",
			 "microsoft.YourPhone",
			 "microsoft.ScreenSketch"]

	@classmethod
	def get_operations(cls):
		return len(cls.apps_dict)
	@staticmethod
	def generate_command_appx(app):
		return "Get-AppxPackage " + app + "|Remove-AppxPackage"
	@classmethod
	def Remove_Appx(cls):
		for command in cls.apps_dict:
			process=subprocess.Popen(["powershell", cls.generate_command_appx(command)],stdout=subprocess.PIPE)
			result=process.communicate()[0]
			print(result)
	
	@classmethod
	def Dlelte_WindowsOld(cls):
		pass
    
	@classmethod
	def Restore_Appx(cls):
		process=subprocess.Popen(["powershell", r"Get-AppxPackage -allusers | foreach {Add-AppxPackage -register \"$($_.InstallLocation)\\appxmanifest.xml\" -DisableDevelopmentMode}"],stdout=subprocess.PIPE)
		result=process.communicate()[0]
		print(result)