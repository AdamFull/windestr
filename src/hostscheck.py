import time, os

class Hosts:
    
    hosts_file = r"C:\\Windows\\System32\\drivers\\etc\\hosts"
    backup_location = "/backup"
    reserved_file = "fls/hosts.txt"

    @classmethod
    def create_backup(cls):
        with open(cls.hosts_file, "r+", encoding = "utf-8") as file:
            if not os.path.exists(cls.backup_location):
                os.mkdir(cls.backup_location)
            with open(cls.backup_location + "/" + "hosts_backup%s" % (round(time.time())), "w+", encoding = "utf-8") as backup:
                backup.writelines(file.read())
                backup.close()
            file.close()

    @classmethod
    def load_backup(cls, backup_loc):
        with open(cls.hosts_file, "w+", encoding = "utf-8") as file:
            with open(backup_loc, "r+", encoding = "utf-8") as backup:
                file.write(backup.read())
                backup.close()
            file.close()

    @staticmethod
    def read_lines(file_name):
        with open(file_name, "r+") as file:
            readed = file.read()
            file.close()
        return readed

    @classmethod
    def file_len(cls, fname):
        num_lines = sum(1 for line in open(cls.hosts_file, "r+", encoding = "utf-8"))
        return num_lines

    @classmethod
    def do(cls):
        if cls.file_len(cls.hosts_file) <= 21:
            cls.create_backup()
            with open(cls.hosts_file, "a+", encoding = "utf-8") as file:
                write = cls.read_lines(cls.reserved_file)
                file.write("\n")
                file.write(write)
                file.close()
        else:
            print("Already added.")