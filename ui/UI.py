# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_win_destr(object):
    def setupUi(self, win_destr):
        win_destr.setObjectName("win_destr")
        win_destr.resize(351, 590)
        self.centralwidget = QtWidgets.QWidget(win_destr)
        self.centralwidget.setObjectName("centralwidget")
        self.disable_list = QtWidgets.QListWidget(self.centralwidget)
        self.disable_list.setGeometry(QtCore.QRect(0, 0, 351, 521))
        self.disable_list.setObjectName("disable_list")
        self.disable_btn = QtWidgets.QPushButton(self.centralwidget)
        self.disable_btn.setGeometry(QtCore.QRect(250, 530, 93, 28))
        self.disable_btn.setObjectName("disable_btn")
        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setGeometry(QtCore.QRect(10, 533, 241, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(True)
        self.progressBar.setObjectName("progressBar")
        win_destr.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(win_destr)
        self.statusbar.setObjectName("statusbar")
        win_destr.setStatusBar(self.statusbar)

        self.retranslateUi(win_destr)
        QtCore.QMetaObject.connectSlotsByName(win_destr)

    def retranslateUi(self, win_destr):
        _translate = QtCore.QCoreApplication.translate
        win_destr.setWindowTitle(_translate("win_destr", "WinDestr"))
        self.disable_btn.setText(_translate("win_destr", "Enter"))

