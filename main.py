from PyQt5 import QtWidgets, QtGui, QtCore
from threading import Thread
from ui.UI import Ui_win_destr
from src.reg_disable import reg_dis
from src.appx import Apps
from src.tools import Tools

# pyuic5 mainui.ui -o UI.py

# registry backup
# develop new interface for "Optimize Internet" page
class MainWindow(QtWidgets.QMainWindow):
    destroy_list = [["Delete Appx", False],
                    ["Disable Defender", True],
                    ["Disable Cortana", True],
                    ["Disable Telemetry", True],
                    ["Disable Windows Auto Update", True]]

    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_win_destr()
        self.ui.setupUi(self)
        self.setFixedSize(self.geometry().width(), self.geometry().height())

        self.load_list(self.ui.disable_list, self.destroy_list)
        self.ui.disable_btn.clicked.connect(self.start_disabling)

        if Tools.isAdmin():
            self.set_statusbar("Administrator rights obtained.", g = 255)
        else:
            self.set_statusbar("Administrator rights denied.", r = 255)
        
    def load_list(self, list_object, pages):
        for page, admin in pages:
            item = QtWidgets.QListWidgetItem(page)
            item.setFlags(item.flags() | QtCore.Qt.ItemIsUserCheckable)
            item.setCheckState(QtCore.Qt.Unchecked)
            if (admin and Tools.isAdmin()) or (not admin and not Tools.isAdmin()) or (not admin and Tools.isAdmin()):
                list_object.addItem(item)

    def start_disabling(self):
        d_list = self.get_list_selected_items(self.ui.disable_list)
        if len(d_list) > 0:
            del_appx_thread = Thread(target = Apps.Remove_Appx)
            for item in d_list:
                if item == "Delete Appx":
                    del_appx_thread.start()
                    self.set_statusbar("Deleting apps, please wait...", r = 255, g = 200)
                    del_appx_thread.join()
                elif item == "Disable Defender":
                    reg_dis.DisableDefender()
                    self.set_statusbar("Disabling Defender...", r = 255, g = 200)
                elif item == "Disable Cortana":
                    reg_dis.DisableCortana()
                    self.set_statusbar("Disabling Cortana...", r = 255, g = 200)
                elif item == "Disable Telemetry":
                    reg_dis.DisableTelemetry()
                    self.set_statusbar("Disabling Telemetry...", r = 255, g = 200)
                elif item == "Disable Windows Auto Update":
                    reg_dis.DisableWindowsAU()
                    self.set_statusbar("Disabling AU...", r = 255, g = 200)
            self.set_statusbar("All done!", g = 200)

    def get_list_selected_items(self, list_object):
        checked_items = []
        for index in range(0, list_object.count()):
            if list_object.item(index).checkState() == QtCore.Qt.Checked:
                checked_items.append(list_object.item(index).text())
        return checked_items

    def set_statusbar(self, text, r = 0, g = 0, b = 0):
        self.ui.statusbar.setStyleSheet("QStatusBar{padding-left:8px;background:rgba(%s,%s,%s,255);color:black;font-weight:bold;}" % (r,g,b))
        self.ui.statusbar.showMessage(text)


def main():
    app = QtWidgets.QApplication([])
    application = MainWindow()
    application.show()
    app.exec()
	
if __name__ == "__main__":
    main()